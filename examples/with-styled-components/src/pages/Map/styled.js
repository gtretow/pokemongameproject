import styled from "styled-components";
import img from "assets/images/pageBackground.png";

export const MapWrapper = styled.div`
  background-image: url(${img});
  background-color: #5dae12;
  background-size: cover;
  justify-content: ${(props) => props.justifyContent || ""};
  flex-direction: ${(props) => props.flexDirection || ""};
  display: ${(props) => props.display || ""};
  height: 100vh;
  max-height: 1800px;
  max-width: 1800px;
  z-index: 900;

  @media (max-width: 810px) {
    background-repeat: no-repeat;
    background-position: center;
    overflow: hidden;
  }

  @media (max-width: 375px) {
    background-repeat: no-repeat;
    background-position: center;
    overflow: hidden;
  }
`;

export const DivContainer = styled.div`
  background-image: ${(props) => props.backgroundImg || ""};
  background-color: ${(props) => props.backgroundColor || ""};
  background-size: ${(props) => props.backgroundSize || "#ffffff"};
  background: ${(props) => props.background || ""};
  max-width: ${(props) => props.maxWidth || ""};
  max-height: ${(props) => props.maxHeight || ""};
  width: ${(props) => props.width || ""};
  height: ${(props) => props.height || ""};
  display: ${(props) => props.display || ""};
  align-items: ${(props) => props.alignItems || ""};
  justify-content: ${(props) => props.justifyContent || ""};
  flex-direction: ${(props) => props.flexDirection || ""};
  position: ${(props) => props.position || ""};
  transform: ${(props) => props.transform || ""};
  top: ${(props) => props.top || ""};
  left: ${(props) => props.left || ""};
  align-self: ${(props) => props.alignSelf || ""};
  margin-top: ${(props) => props.marginTop || ""};
  margin-bottom: ${(props) => props.marginBottom || ""};
`;

export const DivContainerTooltip = styled.div`
  /* tela touch não possui tooltip */

  margin-bottom: 5%;
`;

export const TooltipBox = styled.div`
  opacity: ${(props) => props.opacity || 0};

  ${DivContainerTooltip}:hover & {
    opacity: 1;
  }
`;

export const TooltipImage = styled.img``;

export const NewImg = styled.img`
  width: ${(props) => props.width || ""};
  height: ${(props) => props.height || ""};
  margin-top: ${(props) => props.marginTop || ""};
  margin-bottom: ${(props) => props.marginBottom || ""};
  background: ${(props) => props.background || ""};
  padding-bottom: ${(props) => props.paddingBottom || ""};
  cursor: ${(props) => props.cursor || ""};

  &${TooltipBox}:hover {
    opacity: 1;
  }
`;
