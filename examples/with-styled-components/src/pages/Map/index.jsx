import React, { useState } from "react";
import magnifyingGlass from "../../assets/images/searchTooltip.png";
import toolTipError from "../../assets/images/tooltipError.png";
import NewPokemon from "../../modals/NewPokemon/index";
import Sidebar from "components/Sidebar";
import ashFront from "../../assets/images/ashFront.png";
import * as S from "./styled";
import { useSelector, useDispatch } from "react-redux";
import StatPokemon from "../../modals/StatsPokemon/index";
import CreatePokemon from "../../modals/CreatePokemon/index";

/* import ashRightLeg from "../../assets/images/ashRightLeg.png";
import ashLeftLeg from "../../assets/images/ashLeftLeg.png"; */

const MapPage = () => {
  const [openModalNewPokemon, setOpenModalNewPokemon] = useState(false);
  const [savedPokemon, setSavedPokemon] = useState([]);
  const [openModalStats, setOpenModalStats] = useState(false);
  const [openModalCreate, setOpenModalCreate] = useState(false);
  const [pokemonStats, setPokemonStats] = useState({});
  const [sixPokemons, setSixPokemons] = useState(false);
  const savedPokemonStore = useSelector((state) => state.data);
  const dispatch = useDispatch();
  const sleep = async (ms) => new Promise((r) => setTimeout(r, ms));

  async function modalNewPokemon() {
    if (savedPokemonStore.length < 6) {
      setOpenModalNewPokemon(!openModalNewPokemon);
    } else {
      setSixPokemons(true);
      await sleep(3000);
      setSixPokemons(false);
    }
  }

  function capturePokemonAndCloseModal() {
    setOpenModalNewPokemon(false);
    setSavedPokemon({ savedPokemonStore });
  }

  function openModalPokemonStats() {
    setOpenModalStats(!openModalStats);
  }

  async function openModalCreatePokemon() {
    if (savedPokemonStore.length < 6) {
      setOpenModalCreate(!openModalCreate);
    } else {
      setSixPokemons(true);
      await sleep(3000);
      setSixPokemons(false);
    }
  }

  function getPokemonStats(getPokemon) {
    return { type: "POKEMON_DETAILS", getPokemon };
  }

  function editSinglePokemon(pokemon) {
    setPokemonStats(pokemon);
    setOpenModalStats(true);
    return dispatch(getPokemonStats(pokemon));
  }

  function changePokemonName(newName) {
    let arr = [...pokemonStats];
    arr.newPokemon.name = newName;
    setPokemonStats(arr);
  }

  return (
    <S.MapWrapper
      display="flex"
      justifyContent="center"
      alignItems="center"
      className="map"
    >
      {openModalCreate && <CreatePokemon closeModal={openModalCreatePokemon} />}
      {openModalStats && (
        <StatPokemon
          pokemonStats={pokemonStats.newPokemon}
          changeName={(newName) => changePokemonName(newName)}
          closeModal={openModalPokemonStats}
        />
      )}
      {openModalNewPokemon && (
        <NewPokemon
          closeModal={modalNewPokemon}
          saveAndClose={capturePokemonAndCloseModal}
        />
      )}
      <Sidebar
        openModal={(pokemon) => editSinglePokemon(pokemon)}
        openModalCreatePokemon={openModalCreatePokemon}
      />
      <S.DivContainer
        display="flex"
        justifyContent="center"
        alignItems="center"
        flexDirection="column-reverse"
        overflow="hidden"
      >
        <S.DivContainerTooltip>
          <S.TooltipBox opacity={sixPokemons ? 1 : 0}>
            <S.TooltipImage
              src={sixPokemons ? toolTipError : magnifyingGlass}
            />
          </S.TooltipBox>
          <S.NewImg cursor="pointer" src={ashFront} onClick={modalNewPokemon} />
        </S.DivContainerTooltip>
      </S.DivContainer>
    </S.MapWrapper>
  );
};

export default MapPage;
