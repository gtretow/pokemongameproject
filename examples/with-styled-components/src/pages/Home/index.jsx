import React from "react";
import pkmnLogo from "../../assets/images/pokemonLogo.png";
import { useHistory } from "react-router-dom";
import * as S from "../../assets/styles/mainComponents";
const HomePage = () => {
  const history = useHistory();

  const handleRouteToMap = () => {
    history.push("/map");
  };

  return (
    <S.ContainerComponent>
      <S.ContainerComponent
        display="flex"
        justifyContent="center"
        flexDirection="column"
        alignItems="center"
        position="absolute"
        transform="translate(-50%, -50%)"
        top="50%"
        left="50%"
        background="linear-gradient(to right, rgba(0,214,143,1),rgba(0,214,143,0.48))"
        backgroundSize="cover"
        width="100%"
        height="100%"
      >
        <S.ImgComponent marginBottom="30px" src={pkmnLogo} />
        <S.StandardButton
          onClick={handleRouteToMap}
          color="#fff"
          backgroundColor="#FF3D71"
          borderRadius="35px"
          width="120px"
          height="55px"
          fontWeight="bold"
          fontSize="15px"
          boxShadow="0  15px 15px rgba(0,0,0,0.15)"
          cursor="pointer"
        >
          START
        </S.StandardButton>
      </S.ContainerComponent>
    </S.ContainerComponent>
  );
};

export default HomePage;
