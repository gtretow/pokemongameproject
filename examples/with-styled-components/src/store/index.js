import { createStore, applyMiddleware } from "redux";
const thunk = require("redux-thunk").default;

const INITIAL_STATE = { data: [] };

function pokemons(state = INITIAL_STATE, action) {
  switch (action.type) {
    case "ADD_POKEMON":
      return { ...state, data: [...state.data, action] };
    case "POKEMON_DETAILS":
      return { ...state, data: [...state.data, action] };
    case "DELETE_POKEMON":
      return { data: [...state.data, action] };
    default:
      return state;
  }
}

const store = createStore(pokemons, applyMiddleware(thunk));

export default store;
