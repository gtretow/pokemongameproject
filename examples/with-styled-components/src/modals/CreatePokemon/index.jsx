import React, { useState } from "react";
import * as S from "../../assets/styles/mainComponents";
import closeComponent from "../../assets/images/close.png";
import { useDispatch } from "react-redux";
import defenseImg from "../../assets/images/shield.png";
import swordImg from "../../assets/images/sword.png";
import speedImg from "../../assets/images/speed.png";
import cameraImg from "../../assets/images/camera.png";

export default function CreatePokemon({ saveAndClose, closeModal }) {
  const [newPokemons, setNewPokemons] = useState({
    name: "",
    height: "",
    sprites: [{}],
    weight: "",
    moves: [{}],
    stats: [{}],
  });

  const [createName, setCreateName] = useState("");
  /* const [createWeight, setCreateWeight] = useState("");
  const [createAbilities, setCreateAbilities] = useState([]);
  const [createStats, setCreateStats] = useState([]);
  const [createHeight, setCreateHeight] = useState("");
  const [createType, setCreateTypes] = useState([]);
  const [createHP, setCreateHP] = useState(""); */
  const [errorOnCreation, setErrorOnCreation] = useState(false);

  const dispatch = useDispatch();

  function addPokemonToStore(newPokemon) {
    return { type: "ADD_POKEMON", newPokemon };
  }

  function createPokemon() {
    saveAndClose();

    return dispatch(addPokemonToStore(newPokemons));
  }

  async function getName(event) {
    setCreateName(event.target.value);
  }
  /* 
  function getWeight() {}

  function getMoves() {}

  function getStats() {}

  function getAbilities() {}

  function getHP() {}

  function getType() {}
 */
  return (
    <S.ContainerComponent
      display="fixed"
      position="absolute"
      flexDirection="column"
      justifyContent="center"
      alignItems="center"
      height="100vh"
      width="100%"
      backgroundColor="rgba(0,0,0,0.5)"
      zIndex="500"
    >
      <S.ContainerComponent className="statsPokemon">
        <S.ContainerComponent
          position="absolute"
          left="40%"
          width="300px"
          top="5%"
          height="600px"
          zIndex="1100"
          bottom="10%"
          borderRadius="10px"
          background="linear-gradient(to right,#41EB89,#38F8D3)"
        >
          <S.ContainerComponent
            position="absolute"
            left="86%"
            top="2%"
            borderRadius="50%"
            background="#F7F9FC"
            border="3px solid #00D68F"
            width="30px"
            height="30px"
            display="flex"
            alignItems="center"
            justifyContent="center"
            onClick={closeModal}
          >
            <S.ImgComponent
              cursor="pointer"
              width="15px"
              height="15px"
              src={closeComponent}
            />
          </S.ContainerComponent>
          <S.ContainerComponent
            width="300px"
            height="150px"
            zIndex="1200"
            borderRadius="15px"
          ></S.ContainerComponent>
          <S.ContainerComponent
            position="absolute"
            left="18%"
            top="5%"
            borderRadius="50%"
            background="#F7F9FC"
            border="3px solid #00D68F"
            width="200px"
            height="200px"
            display="flex"
            justifyContent="center"
            alignItems="center"
          >
            <S.ImgComponent width="60px" height="60px" src={cameraImg} />
          </S.ContainerComponent>
          <S.ContainerComponent
            background="#F7F9FC"
            width="300px"
            height="650px"
            zIndex="1200"
            borderRadius="25px 25px 10px 10px"
          >
            <S.ContainerComponent
              position="absolute"
              height="550px"
              width="280px"
              background="#F7F9FC"
              marginLeft="15px"
              display="flex"
              flexDirection="column"
              borderRadius="0 0 10px 10px"
              bottom="0%"
              top="40%"
              overflow="scroll"
              overflowX="hidden"
            >
              <S.ContainerComponent
                height="50px"
                width="150px"
                display="flex"
                flexDirection="column"
                marginBottom="25px"
              >
                <S.ContainerComponent
                  marginBottom="10px"
                  display="flex"
                  flexDirection="column"
                >
                  <S.Text
                    height="100%"
                    fontSize="13px"
                    fontWeight="bold"
                    color="#2E3A59"
                    marginRight="10px"
                  >
                    NOME
                  </S.Text>
                  <S.InputText
                    onChange={getName}
                    value={createName}
                    color="#C4C0B4"
                    fontWeight="bold"
                    padding="14px 10px"
                    placeholder="Nome"
                    fontSize="11px"
                  />
                </S.ContainerComponent>
                <S.ContainerComponent
                  marginBottom="10px"
                  display="flex"
                  flexDirection="column"
                >
                  <S.Text
                    height="100%"
                    fontSize="13px"
                    fontWeight="bold"
                    color="#2E3A59"
                    marginRight="10px"
                  >
                    HP
                  </S.Text>
                  <S.InputText
                    padding="14px 10px"
                    placeholder="HP"
                    type="number"
                    color="#C4C0B4"
                    fontWeight="bold"
                    fontSize="11px"
                  />
                </S.ContainerComponent>
                <S.ContainerComponent
                  marginBottom="10px"
                  display="flex"
                  flexDirection="column"
                >
                  <S.Text
                    height="100%"
                    fontSize="13px"
                    fontWeight="bold"
                    color="#2E3A59"
                    marginRight="10px"
                  >
                    PESO
                  </S.Text>
                  <S.InputText
                    padding="14px 10px"
                    placeholder="PESO"
                    color="#C4C0B4"
                    fontWeight="bold"
                    fontSize="11px"
                  />
                </S.ContainerComponent>
                <S.ContainerComponent
                  marginBottom="10px"
                  display="flex"
                  flexDirection="column"
                >
                  <S.Text
                    height="100%"
                    fontSize="13px"
                    fontWeight="bold"
                    color="#2E3A59"
                    marginRight="10px"
                  >
                    ALTURA
                  </S.Text>
                  <S.InputText
                    padding="14px 10px"
                    placeholder="ALTURA"
                    type="number"
                    color="#C4C0B4"
                    fontWeight="bold"
                    fontSize="11px"
                  />
                </S.ContainerComponent>
                <S.ContainerComponent
                  marginBottom="10px"
                  display="flex"
                  flexDirection="column"
                >
                  <S.Text
                    height="100%"
                    fontSize="13px"
                    fontWeight="bold"
                    color="#2E3A59"
                    marginRight="10px"
                  >
                    TIPO
                  </S.Text>
                  <S.InputText
                    padding="14px 10px"
                    placeholder="Selecione o(s) tipo(s)"
                    fontSize="11px"
                    fontWeight="bold"
                    color="#C4C0B4"
                  />
                </S.ContainerComponent>
                <S.ContainerComponent
                  marginBottom="10px"
                  display="flex"
                  flexDirection="column"
                >
                  <S.Title
                    marginTop="10px"
                    marginBottom="10px"
                    height="100%"
                    fontSize="13px"
                    fontWeight="bold"
                    color="#2E3A59"
                    marginRight="10px"
                  >
                    HABILIDADES
                  </S.Title>
                  <S.InputText
                    padding="14px 10px"
                    fontSize="11px"
                    fontWeight="bold"
                    color="#C4C0B4"
                    placeholder="Habilidade 1"
                    marginBottom="10px"
                  />
                  <S.InputText
                    padding="14px 10px"
                    fontSize="11px"
                    fontWeight="bold"
                    color="#C4C0B4"
                    placeholder="Habilidade 2"
                    marginBottom="10px"
                  />
                  <S.InputText
                    padding="14px 10px"
                    fontSize="11px"
                    fontWeight="bold"
                    color="#C4C0B4"
                    placeholder="Habilidade 3"
                    marginBottom="10px"
                  />
                  <S.InputText
                    padding="14px 10px"
                    fontSize="11px"
                    fontWeight="bold"
                    color="#C4C0B4"
                    placeholder="Habilidade 4"
                  />
                </S.ContainerComponent>

                <S.ContainerComponent
                  marginBottom="10px"
                  display="flex"
                  flexDirection="column"
                >
                  <S.Title
                    marginTop="10px"
                    marginBottom="10px"
                    height="100%"
                    fontSize="13px"
                    fontWeight="bold"
                    color="#2E3A59"
                    marginRight="10px"
                  >
                    ESTATÍSTICAS
                  </S.Title>

                  <S.ContainerComponent display="flex" alignItems="center">
                    <S.ImgComponent
                      height="15px"
                      width="15px"
                      src={defenseImg}
                    />
                    <S.Text
                      marginTop="10px"
                      height="100%"
                      fontSize="13px"
                      fontWeight="bold"
                      color="#2E3A59"
                      marginLeft="5px"
                    >
                      DEFESA
                    </S.Text>
                  </S.ContainerComponent>
                  <S.InputText type="number" />
                </S.ContainerComponent>
                <S.ContainerComponent
                  marginBottom="10px"
                  display="flex"
                  flexDirection="column"
                >
                  <S.ContainerComponent display="flex" alignItems="center">
                    <S.ImgComponent height="15px" width="15px" src={swordImg} />
                    <S.Text
                      marginTop="10px"
                      height="100%"
                      fontSize="13px"
                      fontWeight="bold"
                      color="#2E3A59"
                      marginLeft="5px"
                    >
                      ATAQUE
                    </S.Text>
                  </S.ContainerComponent>
                  <S.InputText type="number" />
                </S.ContainerComponent>
                <S.ContainerComponent
                  marginBottom="10px"
                  display="flex"
                  flexDirection="column"
                >
                  <S.ContainerComponent display="flex" alignItems="center">
                    <S.ImgComponent
                      height="15px"
                      width="15px"
                      src={defenseImg}
                    />
                    <S.Text
                      marginTop="10px"
                      height="100%"
                      fontSize="13px"
                      fontWeight="bold"
                      color="#2E3A59"
                      marginLeft="5px"
                    >
                      DEFESA ESPECIAL
                    </S.Text>
                  </S.ContainerComponent>
                  <S.InputText type="number" />
                </S.ContainerComponent>
                <S.ContainerComponent
                  marginBottom="10px"
                  display="flex"
                  flexDirection="column"
                >
                  <S.ContainerComponent display="flex" alignItems="center">
                    <S.ImgComponent height="15px" width="15px" src={swordImg} />
                    <S.Text
                      marginTop="10px"
                      height="100%"
                      fontSize="13px"
                      fontWeight="bold"
                      color="#2E3A59"
                      marginLeft="5px"
                    >
                      ATAQUE ESPECIAL
                    </S.Text>
                  </S.ContainerComponent>
                  <S.InputText type="number" />
                </S.ContainerComponent>
                <S.ContainerComponent
                  marginBottom="10px"
                  display="flex"
                  flexDirection="column"
                >
                  <S.ContainerComponent display="flex" alignItems="center">
                    <S.ImgComponent height="15px" width="15px" src={speedImg} />
                    <S.Text
                      marginTop="10px"
                      height="100%"
                      fontSize="13px"
                      fontWeight="bold"
                      color="#2E3A59"
                      marginLeft="5px"
                    >
                      VELOCIDADE
                    </S.Text>
                  </S.ContainerComponent>
                  <S.InputText type="number" />
                </S.ContainerComponent>
                <S.ContainerComponent
                  display="flex"
                  justifyContent="center"
                  alignItems="center"
                  width="260px"
                  pointerEvents={errorOnCreation ? "none" : ""}
                >
                  <S.StandardButton
                    width="180px"
                    borderRadius="35px"
                    height="45px"
                    fontWeight="bold"
                    fontSize="15px"
                    backgroundColor="#FF3D71"
                    onClick={createPokemon}
                    cursor="pointer"
                  >
                    CRIAR POKEMON
                  </S.StandardButton>
                </S.ContainerComponent>
              </S.ContainerComponent>
            </S.ContainerComponent>
          </S.ContainerComponent>
        </S.ContainerComponent>
      </S.ContainerComponent>
    </S.ContainerComponent>
  );
}
