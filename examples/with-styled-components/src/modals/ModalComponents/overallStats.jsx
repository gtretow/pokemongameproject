import React from "react";
import * as S from "../../assets/styles/mainComponents";

export default function OverallStats(props) {
  return (
    <S.ContainerComponent
      marginTop="10px"
      marginBottom={props.nomargin ? "" : "10px"}
      display="flex"
    >
      <S.ContainerComponent alignItems="center" width="230px" display="flex">
        <S.ImgComponent width="15px" height="15px" src={props.imgSrc} />
        <S.Text
          fontSize="13px"
          fontWeight="bold"
          color="#2E3A59"
          marginLeft="10px"
        >
          {props.stat}
        </S.Text>
      </S.ContainerComponent>
      <S.ContainerComponent display="flex">
        <S.Title>{props.statNumber}</S.Title>
      </S.ContainerComponent>
    </S.ContainerComponent>
  );
}
