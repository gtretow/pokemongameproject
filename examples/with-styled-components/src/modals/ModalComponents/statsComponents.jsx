import React from "react";
import * as S from "../../assets/styles/mainComponents";

export default function StatsComponents(props) {
  return (
    <S.ContainerComponent
      display="flex"
      flexDirection="column"
      borderLeft={props.borderLeft ? "1px solid #c5cee0" : ""}
      borderRight={props.borderRight ? "1px solid #c5cee0" : ""}
      marginLeft="12px"
    >
      <S.Title
        textAlign="center"
        fontSize="10px"
        fontWeight="bold"
        marginBottom="5px"
        marginTop="10px"
        height="100%"
        color="#2E3A59"
        marginRight={props.marginRight}
        marginLeft={props.marginLeft}
      >
        {props.status}
      </S.Title>
      <S.Text
        fontWeight="bold"
        textAlign="center"
        fontSize="14px"
        marginRight={props.marginRight}
        marginLeft={props.marginLeft}
      >
        {props.statusValue}
      </S.Text>
    </S.ContainerComponent>
  );
}
