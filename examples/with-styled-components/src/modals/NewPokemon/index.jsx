import React, { useEffect, useState } from "react";
import * as S from "../../assets/styles/mainComponents";
import pokeBall from "../../assets/images/pokeball.png";
import axios from "axios";
import StatsComponents from "modals/ModalComponents/statsComponents";
import closeComponent from "../../assets/images/close.png";
import { useDispatch } from "react-redux";
import { buttonDict } from "helpers/pokemonDict";

function randomIntFromInterval(min, max) {
  return Math.floor(Math.random() * (max - min + 1) + min);
}

export default function NewPokemon({ saveAndClose, closeModal }) {
  const [newPokemons, setNewPokemons] = useState({
    name: "",
    height: "",
    sprites: [{}],
    weight: "",
    moves: [{}],
    stats: [{}],
  });

  useEffect(() => {
    let number = randomIntFromInterval(1, 807);
    let response;
    async function fetchData() {
      try {
        response = await axios.get(
          `https://pokeapi.co/api/v2/pokemon/${number}`
        );
      } catch (e) {
        console.error("error getting pokemon", e);
      }

      setNewPokemons(response.data);
    }
    fetchData();
  }, []);

  const dispatch = useDispatch();

  function addPokemonToStore(newPokemon) {
    return { type: "ADD_POKEMON", newPokemon };
  }

  function saveNewPokemon() {
    saveAndClose();

    return dispatch(addPokemonToStore(newPokemons));
  }

  return (
    <S.ContainerComponent
      display="fixed"
      position="absolute"
      flexDirection="column"
      justifyContent="center"
      alignItems="center"
      height="100vh"
      width="100%"
      backgroundColor="rgba(0,0,0,0.5)"
      zIndex="500"
    >
      <S.ContainerComponent className="createPokemon">
        <S.ContainerComponent
          position="absolute"
          left="40%"
          width="300px"
          top="13%"
          height="600px"
          zIndex="1100"
          bottom="10%"
          borderRadius="10px"
          background="linear-gradient(to right,#41EB89,#38F8D3)"
        >
          <S.ContainerComponent
            position="absolute"
            left="80%"
            top="5%"
            borderRadius="50%"
            background="#F7F9FC"
            border="3px solid #00D68F"
            width="30px"
            height="30px"
            display="flex"
            alignItems="center"
            justifyContent="center"
            className="close"
            onClick={closeModal}
          >
            <S.ImgComponent
              cursor="pointer"
              width="15px"
              height="15px"
              src={closeComponent}
            />
          </S.ContainerComponent>
          <S.ContainerComponent
            width="300px"
            height="150px"
            zIndex="1200"
            borderRadius="15px"
          ></S.ContainerComponent>
          <S.ContainerComponent
            position="absolute"
            left="18%"
            top="5%"
            borderRadius="50%"
            background="#F7F9FC"
            border="3px solid #00D68F"
            width="200px"
            height="200px"
            display="flex"
            justifyContent="center"
            alignItems="center"
          >
            <S.ImgComponent
              height="170px"
              src={newPokemons.sprites.front_default}
            />
          </S.ContainerComponent>

          <S.ContainerComponent
            background="#F7F9FC"
            width="300px"
            height="450px"
            zIndex="1200"
            borderRadius="25px 25px 10px 10px"
          >
            <S.ContainerComponent
              position="absolute"
              height="350px"
              width="300px"
              background="#F7F9FC"
              alignItems="center"
              display="flex"
              flexDirection="column"
              borderRadius="0 0 10px 10px"
              bottom="3%"
            >
              <S.ContainerComponent
                height="50px"
                width="150px"
                display="flex"
                flexDirection="column"
                marginBottom="25px"
              >
                <S.ContainerComponent
                  justifyContent="center"
                  alignItems="center"
                  marginBottom="10px"
                  display="flex"
                  textAlign="center"
                  width="100%"
                  fontSize="13px"
                  fontWeight="bold"
                  color="#2E3A59"
                >
                  <S.Title
                    marginLeft="23px"
                    textAlign="center"
                    marginRight="10px"
                  >
                    {newPokemons.name.toUpperCase()}
                  </S.Title>
                </S.ContainerComponent>
                <S.ContainerComponent display="flex">
                  <S.ContainerComponent marginRight="5px">
                    <StatsComponents
                      status="HP"
                      statusValue={newPokemons.stats[0].base_stat}
                    />
                  </S.ContainerComponent>
                  <StatsComponents
                    borderLeft
                    borderRight
                    marginRight="10px"
                    marginLeft="10px"
                    status="ALTURA"
                    statusValue={`${newPokemons.height}m`}
                  />
                  <StatsComponents
                    marginRight="15px"
                    status="PESO"
                    statusValue={`${newPokemons.weight}kg`}
                  />
                </S.ContainerComponent>
              </S.ContainerComponent>

              <S.ContainerComponent
                marginBottom="20px"
                display="flex"
                flexDirection="column"
              >
                <S.Title
                  marginTop="10px"
                  marginBottom="10px"
                  textAlign="center"
                  height="100%"
                  fontSize="13px"
                  fontWeight="bold"
                  color="#2E3A59"
                >
                  TIPO
                </S.Title>
                <S.ContainerComponent display="flex">
                  {(newPokemons.types || []).map((pkmntypes, idx) => {
                    return (
                      <S.StandardButton
                        key={idx}
                        borderRadius="35px"
                        width="85px"
                        height="30px"
                        fontWeight="bold"
                        fontSize="15px"
                        marginRight="5px"
                        backgroundColor={
                          buttonDict[pkmntypes.type.name].bgColor
                        }
                      >
                        {buttonDict[pkmntypes.type.name].name}
                      </S.StandardButton>
                    );
                  })}
                </S.ContainerComponent>
                <S.ContainerComponent></S.ContainerComponent>
              </S.ContainerComponent>

              <S.ContainerComponent display="flex" flexDirection="column">
                <S.Title
                  marginTop="10px"
                  marginBottom="10px"
                  textAlign="center"
                  height="100%"
                  fontSize="13px"
                  fontWeight="bold"
                  color="#2E3A59"
                >
                  HABILIDADES
                </S.Title>
                <S.ContainerComponent display="flex" flexDirection="row">
                  {(newPokemons.abilities || []).map((ability, idx) => {
                    return (
                      <S.Text
                        key={idx}
                        textAlign="center"
                        marginRight="4px"
                      >{`${ability.ability.name.toUpperCase()},`}</S.Text>
                    );
                  })}
                </S.ContainerComponent>
              </S.ContainerComponent>
              <S.ContainerComponent
                width="250px"
                display="flex"
                flexDirection="column"
              ></S.ContainerComponent>
              <S.ContainerComponent
                display="flex"
                justifyContent="center"
                alignItems="center"
              >
                <S.ImgComponent
                  marginTop="27px"
                  onClick={saveNewPokemon}
                  cursor="pointer"
                  src={pokeBall}
                ></S.ImgComponent>
              </S.ContainerComponent>
            </S.ContainerComponent>
          </S.ContainerComponent>
        </S.ContainerComponent>
      </S.ContainerComponent>
    </S.ContainerComponent>
  );
}
