import React, { useState } from "react";
import * as S from "../../assets/styles/mainComponents";
import editIcon from "../../assets/images/editIcon.png";
import StatsComponents from "../../modals/ModalComponents/statsComponents";
import closeComponent from "../../assets/images/close.png";
import OverallStats from "../ModalComponents/overallStats";
import defenseImg from "../../assets/images/shield.png";
import swordImg from "../../assets/images/sword.png";
import speedImg from "../../assets/images/speed.png";
import { useSelector, useDispatch } from "react-redux";
import { buttonDict } from "helpers/pokemonDict";

export default function PokemonStats(props) {
  const [pokemonList, setPokemonList] = useState([]);
  const savedPokemonStore = useSelector((state) => state.data);
  const dispatch = useDispatch();

  function deletePokemonFromStore(newPokemon) {
    return { type: "DELETE_POKEMON", newPokemon };
  }

  const releasePokemon = () => {
    props.closeModal();
    let pokemonsCaught = savedPokemonStore.filter(
      (store) => store.type === "ADD_POKEMON"
    );
    const pokemonList = pokemonsCaught.filter((pkmn) => {
      return pkmn.newPokemon.id !== props.pokemonStats.id;
    });
    setPokemonList(pokemonList);

    deletePokemon();
  };

  function deletePokemon() {
    return dispatch(deletePokemonFromStore(pokemonList.newPokemon));
  }

  return (
    <S.ContainerComponent
      display="fixed"
      position="absolute"
      flexDirection="column"
      justifyContent="center"
      alignItems="center"
      height="100vh"
      width="100%"
      backgroundColor="rgba(0,0,0,0.5)"
      zIndex="500"
    >
      <S.ContainerComponent className="statsPokemon">
        <S.ContainerComponent
          position="absolute"
          left="40%"
          width="300px"
          top="8%"
          height="600px"
          zIndex="1100"
          bottom="10%"
          borderRadius="10px"
          background="linear-gradient(to right,#41EB89,#38F8D3)"
        >
          <S.ContainerComponent
            position="absolute"
            left="80%"
            top="5%"
            borderRadius="50%"
            background="#F7F9FC"
            border="3px solid #00D68F"
            width="30px"
            height="30px"
            display="flex"
            alignItems="center"
            justifyContent="center"
            onClick={props.closeModal}
          >
            <S.ImgComponent
              cursor="pointer"
              width="15px"
              height="15px"
              src={closeComponent}
            />
          </S.ContainerComponent>
          <S.ContainerComponent
            width="300px"
            height="150px"
            zIndex="1200"
            borderRadius="15px"
          ></S.ContainerComponent>
          <S.ContainerComponent
            position="absolute"
            left="18%"
            top="5%"
            borderRadius="50%"
            background="#F7F9FC"
            border="3px solid #00D68F"
            width="200px"
            height="200px"
            display="flex"
            justifyContent="center"
            alignItems="center"
          >
            <S.ImgComponent
              height="170px"
              src={props.pokemonStats.sprites.front_default}
            />
          </S.ContainerComponent>

          <S.ContainerComponent
            background="#F7F9FC"
            width="300px"
            height="650px"
            zIndex="1200"
            borderRadius="25px 25px 10px 10px"
          >
            <S.ContainerComponent
              position="absolute"
              height="350px"
              width="300px"
              background="#F7F9FC"
              alignItems="center"
              display="flex"
              flexDirection="column"
              borderRadius="0 0 10px 10px"
              bottom="3%"
            >
              <S.ContainerComponent
                height="50px"
                width="150px"
                display="flex"
                flexDirection="column"
                marginBottom="25px"
              >
                <S.ContainerComponent
                  justifyContent="center"
                  alignItems="center"
                  marginBottom="10px"
                  display="flex"
                  textAlign="center"
                  width="100%"
                  fontSize="13px"
                  fontWeight="bold"
                  color="#2E3A59"
                >
                  <S.Title
                    marginLeft="23px"
                    textAlign="center"
                    marginRight="10px"
                  >
                    {props.pokemonStats.name.toUpperCase()}
                  </S.Title>
                  <S.ImgComponent
                    cursor="pointer"
                    width="15px"
                    height="15px"
                    src={editIcon}
                  />
                </S.ContainerComponent>
                <S.ContainerComponent display="flex">
                  <S.ContainerComponent marginRight="5px">
                    <StatsComponents
                      status="HP"
                      statusValue={props.pokemonStats.stats[0].base_stat}
                    />
                  </S.ContainerComponent>
                  <StatsComponents
                    borderLeft
                    borderRight
                    marginRight="10px"
                    marginLeft="10px"
                    status="ALTURA"
                    statusValue={`${props.pokemonStats.height}m`}
                  />
                  <StatsComponents
                    marginRight="15px"
                    status="PESO"
                    statusValue={`${props.pokemonStats.weight}kg`}
                  />
                </S.ContainerComponent>
              </S.ContainerComponent>

              <S.ContainerComponent
                marginBottom="20px"
                display="flex"
                flexDirection="column"
              >
                <S.Title
                  marginTop="10px"
                  marginBottom="10px"
                  textAlign="center"
                  height="100%"
                  fontSize="13px"
                  fontWeight="bold"
                  color="#2E3A59"
                >
                  TIPO
                </S.Title>
                <S.ContainerComponent display="flex">
                  {(props.pokemonStats.types || []).map((pkmntypes, idx) => {
                    return (
                      <S.StandardButton
                        key={idx}
                        borderRadius="35px"
                        width="80px"
                        height="30px"
                        fontWeight="bold"
                        fontSize="15px"
                        marginRight="10px"
                        backgroundColor={
                          buttonDict[pkmntypes.type.name].bgColor
                        }
                      >
                        {buttonDict[pkmntypes.type.name].name}
                      </S.StandardButton>
                    );
                  })}
                </S.ContainerComponent>
                <S.ContainerComponent></S.ContainerComponent>
              </S.ContainerComponent>

              <S.ContainerComponent display="flex" flexDirection="column">
                <S.ContainerComponent flexDirection="row">
                  <S.Title
                    marginTop="10px"
                    marginBottom="10px"
                    textAlign="center"
                    height="100%"
                    fontSize="13px"
                    fontWeight="bold"
                    color="#2E3A59"
                    padding="0 10px"
                    display="flex"
                    flexDirection="row"
                    className="textLines"
                  >
                    HABILIDADES
                  </S.Title>
                </S.ContainerComponent>
                <S.ContainerComponent display="flex" flexDirection="row">
                  {(props.pokemonStats.abilities || []).map((ability, idx) => {
                    return (
                      <S.Text key={idx} textAlign="center" marginRight="4px">
                        {`${ability.ability.name.toUpperCase()}`}
                      </S.Text>
                    );
                  })}
                </S.ContainerComponent>
              </S.ContainerComponent>
              <S.ContainerComponent
                width="250px"
                display="flex"
                flexDirection="column"
              >
                <S.ContainerComponent>
                  <S.Title
                    marginTop="10px"
                    marginBottom="10px"
                    textAlign="center"
                    height="100%"
                    fontSize="13px"
                    fontWeight="bold"
                    color="#2E3A59"
                    className="textLines"
                  >
                    ESTATÍSTICAS
                  </S.Title>
                </S.ContainerComponent>

                <S.ContainerComponent
                  display="flex"
                  justifyContent="space-between"
                  flexDirection="column"
                  marginBottom="30px"
                >
                  <OverallStats
                    imgSrc={defenseImg}
                    stat="DEFESA"
                    statNumber={props.pokemonStats.stats[2].base_stat}
                  />
                  <OverallStats
                    imgSrc={swordImg}
                    stat="ATAQUE"
                    statNumber={props.pokemonStats.stats[1].base_stat}
                  />
                  <OverallStats
                    imgSrc={defenseImg}
                    stat="DEFESA ESPECIAL"
                    statNumber={props.pokemonStats.stats[4].base_stat}
                  />
                  <OverallStats
                    imgSrc={swordImg}
                    stat="ATAQUE ESPECIAL"
                    statNumber={props.pokemonStats.stats[3].base_stat}
                  />
                  <OverallStats
                    nomargin
                    imgSrc={speedImg}
                    stat="VELOCIDADE"
                    statNumber={props.pokemonStats.stats[5].base_stat}
                  />
                </S.ContainerComponent>
                <S.ContainerComponent
                  display="flex"
                  justifyContent="center"
                  alignItems="center"
                >
                  <S.StandardButton
                    width="180px"
                    borderRadius="35px"
                    height="45px"
                    fontWeight="bold"
                    fontSize="15px"
                    backgroundColor="#FF3D71"
                    onClick={releasePokemon}
                    cursor="pointer"
                  >
                    LIBERAR POKEMON
                  </S.StandardButton>
                </S.ContainerComponent>
              </S.ContainerComponent>
            </S.ContainerComponent>
          </S.ContainerComponent>
        </S.ContainerComponent>
      </S.ContainerComponent>
    </S.ContainerComponent>
  );
}
