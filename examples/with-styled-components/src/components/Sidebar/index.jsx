import React, { useEffect, useState } from "react";

import Button from "components/Button";

import iconPlus from "assets/images/plus.png";

import * as S from "./styled";
import { ImgComponent } from "../../assets/styles/mainComponents";

import { useSelector } from "react-redux";

const Sidebar = (props) => {
  const [pokemonBelt, setPokemonBelt] = useState([]);
  const savedPokemonStore = useSelector((state) => state.data);

  useEffect(() => {
    let pokemonsCaught = savedPokemonStore.filter(
      (store) => store.type === "ADD_POKEMON"
    );
    async function getPokemons() {
      await setPokemonBelt(pokemonsCaught);
    }

    getPokemons();
  }, [savedPokemonStore]);

  return (
    <S.SideBarWrapper>
      <S.SideBarList>
        {pokemonBelt.map((capturedPokemons, key) => {
          return (
            <S.SideBarItem
              idx={key}
              onClick={() => props.openModal(capturedPokemons)}
            >
              <ImgComponent
                width="70px"
                height="70px"
                src={capturedPokemons.newPokemon.sprites.front_default}
              />
            </S.SideBarItem>
          );
        })}
        {pokemonBelt.length < 1 && <S.SideBarItem>?</S.SideBarItem>}
        {pokemonBelt.length < 2 && <S.SideBarItem>?</S.SideBarItem>}
        {pokemonBelt.length < 3 && <S.SideBarItem>?</S.SideBarItem>}
        {pokemonBelt.length < 4 && <S.SideBarItem>?</S.SideBarItem>}
        {pokemonBelt.length < 5 && <S.SideBarItem>?</S.SideBarItem>}
        {pokemonBelt.length < 6 && <S.SideBarItem>?</S.SideBarItem>}
      </S.SideBarList>

      <Button onClick={props.openModalCreatePokemon} icon={iconPlus} />
    </S.SideBarWrapper>
  );
};

export default Sidebar;
