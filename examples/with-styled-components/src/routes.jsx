import React from "react";
import { Route, BrowserRouter, Switch, Redirect } from "react-router-dom";
import Home from "./pages/Home/index";
import MapPage from "pages/Map";
const Routes = () => {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact component={Home} path="/" />
        <Route exact component={MapPage} path="/map" />
        <Redirect to="/" />
      </Switch>
    </BrowserRouter>
  );
};

export default Routes;
