import styled from "styled-components";

export const ContainerComponent = styled.div`
  background-image: ${(props) => props.backgroundImg || ""};
  background-color: ${(props) => props.backgroundColor || ""};
  background-size: ${(props) => props.backgroundSize || "#ffffff"};
  background: ${(props) => props.background || ""};
  max-width: ${(props) => props.maxWidth || ""};
  max-height: ${(props) => props.maxHeight || ""};
  width: ${(props) => props.width || ""};
  height: ${(props) => props.height || ""};
  display: ${(props) => props.display || ""};
  align-items: ${(props) => props.alignItems || ""};
  justify-content: ${(props) => props.justifyContent || ""};
  flex-direction: ${(props) => props.flexDirection || ""};
  position: ${(props) => props.position || ""};
  transform: ${(props) => props.transform || ""};
  top: ${(props) => props.top || ""};
  bottom: ${(props) => props.bottom || ""};
  left: ${(props) => props.left || ""};
  right: ${(props) => props.right || ""};
  align-self: ${(props) => props.alignSelf || ""};
  z-index: ${(props) => props.zIndex || ""};
  padding-right: ${(props) => props.paddingRight || ""};
  padding-left: ${(props) => props.paddingLeft || ""};
  object-fit: ${(props) => props.objectFit || ""};
  border-radius: ${(props) => props.borderRadius || ""};
  border: ${(props) => props.border || ""};
  margin-left: ${(props) => props.marginLeft || ""};
  margin-top: ${(props) => props.marginTop || ""};
  margin-right: ${(props) => props.marginRight || ""};
  margin-bottom: ${(props) => props.marginBottom || ""};
  align-self: ${(props) => props.alignSelf || ""};
  border-left: ${(props) => props.borderLeft || ""};
  border-right: ${(props) => props.borderRight || ""};
  overflow: ${(props) => props.overflow || ""};
  overflow-y: ${(props) => props.overflowY || ""};
  overflow-x: ${(props) => props.overflowX || ""};
  pointer-events: ${(props) => props.pointerEvents || ""};
  scrollbar-width: thin;
  flex: ${(props) => props.flex || ""};

  @media (max-width: 810px) {
    background-repeat: no-repeat;
    background-position: center;
    max-height: 1080px;

    .circle {
      top: 30%;
      left: 38%;
    }

    .close {
      top: 2%;
      left: 84%;
    }
  }

  @media (max-width: 375px) {
    background-repeat: no-repeat;
    background-position: center top;
    max-height: 812px;
    .circle {
      top: 16%;
      left: 25%;
    }

    .close {
    }

    .statsPokemon {
      top: 0;
      left: 10%;
      position: absolute;
    }

    .createPokemon {
      top: 11%;
      left: 10%;
      position: absolute;
    }
  }
`;

export const ImgComponent = styled.img`
  width: ${(props) => props.width || ""};
  height: ${(props) => props.height || ""};
  margin-top: ${(props) => props.marginTop || ""};
  margin-bottom: ${(props) => props.marginBottom || ""};
  background: ${(props) => props.background || ""};
  src: ${(props) => props.src || ""};
  cursor: ${(props) => props.cursor || "white"};
  display: ${(props) => props.display || ""};

  @media (max-width: 810px) {
    background-repeat: no-repeat;
    background-position: center;
    overflow: hidden;
  }

  @media (max-width: 375px) {
    background-repeat: no-repeat;
    background-position: center;
  }
`;

export const StandardButton = styled.button`
  display: ${(props) => props.display || ""};
  width: ${(props) => props.width || "93px"};
  height: ${(props) => props.height || "30px"};
  color: ${(props) => props.color || "white"};
  background: ${(props) => props.backgroundColor || ""};
  border-radius: ${(props) => props.borderRadius || "#75c181"};
  border: ${(props) => props.border || "none"};
  font-weight: ${(props) => props.fontWeight || "normal"};
  font-size: ${(props) => props.fontSize || ""};
  box-shadow: ${(props) => props.boxShadow || ""};
  cursor: ${(props) => props.cursor || ""};
  margin-right: ${(props) => props.marginRight || ""};
`;

export const Title = styled.h2`
  font-weight: ${(props) => props.fontWeight || "normal"};
  font-size: ${(props) => props.fontSize || ""};
  font-style: ${(props) => props.fontStyle || "normal"};
  font-stretch: ${(props) => props.fontStretch || ""};
  text-align: ${(props) => props.textAlign || ""};
  color: ${(props) => props.color || ""};
  display: ${(props) => props.display || ""};
  width: ${(props) => props.width || ""};
  height: ${(props) => props.height || ""};
  flex-direction: ${(props) => props.flexDirection || ""};
  margin: ${(props) => props.margin || ""};
  margin-left: ${(props) => props.marginLeft || ""};
  margin-top: ${(props) => props.marginTop || ""};
  margin-right: ${(props) => props.marginRight || ""};
  margin-bottom: ${(props) => props.marginBottom || ""};
  align-self: ${(props) => props.alignSelf || ""};
  margin-left: ${(props) => props.marginLeft || ""};
  margin-top: ${(props) => props.marginTop || ""};
  margin-right: ${(props) => props.marginRight || ""};
  margin-bottom: ${(props) => props.marginBottom || ""};
  border-bottom: ${(props) => props.borderBottom || ""};
  line-height: ${(props) => props.lineHeight || ""};

  .textLines {
    &:before {
      content: "";
      flex: 1 1;
      border-bottom: 1px solid;
      margin: auto;
      margin-right: 10px;
    }

    &:after {
      content: "";
      flex: 1 1;
      border-bottom: 1px solid;
      margin: auto;
      margin-left: 10px;
    }
  }
`;

export const Text = styled.p`
  font-weight: ${(props) => props.fontWeight || "normal"};
  font-size: ${(props) => props.fontSize || ""};
  font-style: ${(props) => props.fontStyle || "normal"};
  font-stretch: ${(props) => props.fontStretch || ""};
  text-align: ${(props) => props.textAlign || ""};
  color: ${(props) => props.color || ""};
  margin: ${(props) => props.margin || ""};
  margin-left: ${(props) => props.marginLeft || ""};
  margin-top: ${(props) => props.marginTop || ""};
  margin-right: ${(props) => props.marginRight || ""};
  margin-bottom: ${(props) => props.marginBottom || ""};

  .comma {
    ::after {
      content: ", ";
    }

    ::last-of-type:after {
      content: "";
    }
  }
`;

export const InputText = styled.input`
  border-radius: 3px;
  border: 1px solid rgb(226, 224, 224);
  width: 270px;
  height: 25px;
  font-weight: ${(props) => props.fontWeight || "normal"};
  font-size: ${(props) => props.fontSize || ""};
  font-style: ${(props) => props.fontStyle || "normal"};
  font-stretch: ${(props) => props.fontStretch || ""};
  text-align: ${(props) => props.textAlign || ""};
  color: ${(props) => props.color || ""};
  margin: ${(props) => props.margin || ""};
  margin-left: ${(props) => props.marginLeft || ""};
  margin-top: ${(props) => props.marginTop || ""};
  margin-right: ${(props) => props.marginRight || ""};
  margin-bottom: ${(props) => props.marginBottom || ""};
  max-width: ${(props) => props.maxWidth || ""};
  max-height: ${(props) => props.maxHeight || ""};
  display: ${(props) => props.display || ""};
  align-items: ${(props) => props.alignItems || ""};
  justify-content: ${(props) => props.justifyContent || ""};
  flex-direction: ${(props) => props.flexDirection || ""};
  position: ${(props) => props.position || ""};
  transform: ${(props) => props.transform || ""};
  top: ${(props) => props.top || ""};
  bottom: ${(props) => props.bottom || ""};
  left: ${(props) => props.left || ""};
  align-self: ${(props) => props.alignSelf || ""};
  z-index: ${(props) => props.zIndex || ""};
  padding-right: ${(props) => props.paddingRight || ""};
  padding-left: ${(props) => props.paddingLeft || ""};
  object-fit: ${(props) => props.objectFit || ""};
  border-radius: ${(props) => props.borderRadius || ""};
  border: ${(props) => props.border || ""};
  align-self: ${(props) => props.alignSelf || ""};
  border-left: ${(props) => props.borderLeft || ""};
  padding: ${(props) => props.padding || ""};
  border-right: ${(props) => props.borderRight || ""};
`;

export const InputSelect = styled.select``;

export const HrElement = styled.hr`
  border: 1px solid #7f8488;
`;
