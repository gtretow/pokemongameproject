# PokemonProject

Formatos usados como base para responsividade:

375x812 - mobile
810x1080 - ipad
1600x900 - monitor

Fluxograma inicial: https://drive.google.com/file/d/1i5y3nWqyQrUUrj1jX9yUQaKlIVswrYO2/view?usp=sharing

## Instruções

git clone https://gtretow@bitbucket.org/gtretow/pokemongameproject.git

npm install

npm start

###

## Desafios

    [x] Landing page com um botão para iniciar a aplicação (qualquer url inexistente deve redirecionar o usuário para essa landing page).
    [x] Personagem no centro da página.
    [x] Barra na esquerda indicando quantos Pokémons ele já capturou (limite de 6) + botão de criação.
    [x] Ao passar o mouse no personagem é exibido o tooltip correspondente.
    [x] Ao clicar no personagem é feita uma busca por um Pokémon aleatório (id de 1 a 807).
    [x] Com o resultado da busca é aberto um modal mostrando os detalhes do Pokémon.
    [x] Usuário tem a opção de capturá-lo, clicando na pokébola, ou fechar o modal.
    [x] Se ele capturar o Pokémon, esse Pokémon é exibido na SideBar e o modal de captura desaparece.
    [x] Usuário pode capturar até 6 Pokémons.
    [x] Selecionando qualquer Pokémon na SideBar o usuário pode ver os detalhes do Pokémon.
    [x] O(s) tipo(s) do Pokémon deve ser traduzido (ex: water => Água).
    [ ] Usuário pode editar SOMENTE o nome de um Pokémon que foi capturado.
    [ ] Na SideBar o usuário tem a possibilidade de criar um Pokémon (um Pokémon pode ter no máximo 2 "tipos").
    [ ] O formulário de criação de Pokémon deve conter validações em todos os campos.
    [ ] Caso algum campo não esteja preenchido, o botão de criação deve ficar bloqueado.
    [ ] Para um Pokémon criado o usuário pode editar qualquer informação ou liberá-lo.
    [ ] Sempre que liberar um Pokémon é possível capturar outro através da busca ou criar um customizado.
    [x] Caso as 6 posições estejam ocupadas o usuário não pode mais buscar nem criar novos Pokémons.
    [x] Responsividade para resoluções desktop e mobile. (Ex: 1280 x 720, 360 x 740)
